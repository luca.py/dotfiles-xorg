--vim.cmd.colorscheme "decay-dark"
--vim.cmd.colorscheme "decay-light"
--vim.cmd.colorscheme "decay-default"
--vim.cmd.colorscheme "decayce"
--vim.cmd("colorscheme decay-dark")

local decay = require("decay")

local opt = vim.opt
local cmd = vim.cmd

opt.background = "dark"

decay.setup({
  style = "default",

  -- enables italics in code keywords & comments.
  italics = {
    code = true,
    comments = true,
  },

	-- enables contrast when using nvim tree.
	nvim_tree = {
		contrast = false
	},
	cmp = { 
		block_kind = false 
	},
})

cmd.colorscheme "decay"
