-- ~/.config/nvim/lua/config/plugins/cmp.lua

-- Require necessary modules
local lsp_zero = require('lsp-zero')
local cmp = require('cmp')
local luasnip = require('luasnip')

-- Extend cmp with lsp-zero settings
lsp_zero.extend_cmp()
local cmp_action = lsp_zero.cmp_action()

-- Set up nvim-cmp
cmp.setup({
    formatting = lsp_zero.cmp_format({ details = true }),
    mapping = {
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Scroll up
        ['<C-d>'] = cmp.mapping.scroll_docs(4),  -- Scroll down
        ['<C-f>'] = cmp_action.luasnip_jump_forward(),
        ['<C-b>'] = cmp_action.luasnip_jump_backward(),
        ['<CR>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        }),
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback() -- fallback to default behavior
            end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback() -- fallback to default behavior
            end
        end, { 'i', 's' }),
    },
	formatting = {
			format = require("nvim-highlight-colors").format
	},
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body) -- Use LuaSnip to expand snippets
        end,
    },
    sources = {
        { name = 'nvim_lsp' },  -- LSP completion
        { name = 'luasnip' },    -- Snippet completion
    },
})

-- Additional configuration if needed
-- This is optional; you can add more configuration here if necessary.
