local map = vim.keymap.set
local opts = { noremap = true, silent = true }

-- space bar leader key
vim.g.mapleader = " "

-- buffers
vim.keymap.set("n", "<leader>n", ":bn<cr>")
vim.keymap.set("n", "<leader>p", ":bp<cr>")
vim.keymap.set("n", "<leader>x", ":bd<cr>")

-- yank to clipboard
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])

-- black python formatting
vim.keymap.set("n", "<leader>fmp", ":silent !black %<cr>")

--> nvim tree mappings <--
vim.keymap.set("n", "<leader>e", ":NvimTreeFindFileToggle<cr>")

--> telescope mappings <--
map("n", "<leader>fs", ":Telescope find_files<cr>", opts)
map("n", "<leader>fp", ":Telescope git_files<cr>", opts)
map("n", "<leader>fz", ":Telescope live_grep<cr>", opts)
map("n", "<leader>fo", ":Telescope oldfiles<cr>", opts)


-- format code using LSP
vim.keymap.set("n", "<leader>fmd", vim.lsp.buf.format)

-- markdown preview
vim.keymap.set("n", "<leader>mp", ":MarkdownPreviewToggle<cr>")

-- nvim-comment
vim.keymap.set({"n", "v"}, "<leader>/", ":CommentToggle<cr>")
