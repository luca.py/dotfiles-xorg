-- Colors :
colors = {}
-- Dark colors
colors.black 									= "#222222"
colors.red 										= "#a54242"
colors.green 									= "#8c9440"
colors.yellow 									= "#de935f"
colors.blue 									= "#548D91"
colors.magenta 									= "#85678f"
colors.cyan 									= "#5e8d87"
colors.white 									= "#acacac"
-- Bright colors
colors.brightblack 								= "#303030"
colors.brightred 								= "#cc6666"
colors.brightgreen 								= "#b5bd68"
colors.brightyellow 							= "#f0c674"
colors.brightblue 								= "#81a2be"
colors.brightmagenta 							= "#b294bb"
colors.brightcyan 								= "#8abeb7"
colors.brightwhite 								= "#c5c8c6"
-- Other
colors.transparent 								= "#00000000"
colors.container 								= "#2B2B2B"
colors.main_scheme								= "#f0544c"
colors.main_transparent 						= "#212121CC"

return colors
