-- Colors :
colors = {}
-- Dark colors
colors.black 									= "#1B2224"
colors.red 										= "#a54242"
colors.green 									= "#8c9440"
colors.yellow 									= "#de935f"
colors.blue 									= "#548D91"
colors.magenta 									= "#85678f"
colors.cyan 									= "#5e8d87"
colors.white 									= "#acacac"
-- Bright colors
colors.brightblack 								= "#263033"
colors.brightred 								= "#cc6666"
colors.brightgreen 								= "#b5bd68"
colors.brightyellow 							= "#f0c674"
colors.brightblue 								= "#81a2be"
colors.brightmagenta 							= "#b294bb"
colors.brightcyan 								= "#8abeb7"
colors.brightwhite 								= "#c5c8c6"
-- Other
colors.transparent 								= "#00000000"
colors.container 								= "#222B2E"
colors.main_scheme								= "#2F9B85"
colors.main_transparent 						= "#1A2123CC"

return colors
