-- Colors :
colors = {}
-- Dark colors
colors.black 									= "#1c1f24"
colors.red 										= "#ff6c6b"
colors.green 									= "#98be65"
colors.yellow 									= "#da8548"
colors.blue 									= "#4390c4"
colors.magenta 									= "#c678dd"
colors.cyan 									= "#5699af"
colors.white 									= "#acacac"
-- Bright colors
colors.brightblack 								= "#262A30"
colors.brightred 								= "#da8548"
colors.brightgreen 								= "#4db5bd"
colors.brightyellow 							= "#ecbe7b"
colors.brightblue 								= "#51afef"
colors.brightmagenta 							= "#a9a1e1"
colors.brightcyan 								= "#46d9ff"
colors.brightwhite 								= "#dfdfdf"
-- Other
colors.transparent 								= "#00000000"
colors.container 								= "#24272E"
colors.main_scheme								= "#4390c4"
colors.main_transparent 						= "#1D2025CC"

return colors
