-- ## Keybindings ##
-- ~~~~~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful         = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")

-- vars
-- ~~~~~~~~~

-- modkey
local modkey    = "Mod4"
-- modifier keys
local shift     = "Shift"
local ctrl      = "Control"
local alt       = "Mod1"

-- Default Applications :
terminal = "kitty"
web_browser = "brave"
file_manager = "pcmanfm"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Configurations
-- ~~~~~~~~~~~~~~

-- # Mouse bindings :
awful.mouse.append_global_mousebindings({
    awful.button({ }, 3, function () mymainmenu:toggle() end)
})

client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({
        awful.button({ }, 1, function (c)
            c:activate { context = "mouse_click" }
        end),
        awful.button({ modkey }, 1, function (c)
            c:activate { context = "mouse_click", action = "mouse_move"  }
        end),
        awful.button({ modkey }, 3, function (c)
            c:activate { context = "mouse_click", action = "mouse_resize"}
        end),
    })
end)

-- Sloppy focus :
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)

-- # Key bindings :

-- General Awesome keys
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,       }, "s", hotkeys_popup.show_help, {description = "show help", group = "awesome"}),
    awful.key({ modkey,       }, "w", function () mymainmenu:show() end, {description = "show main menu", group = "awesome"}),
    awful.key({ modkey, ctrl  }, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, shift }, "q", awesome.quit, {description = "quit awesome", group = "awesome"}),
    awful.key({ modkey,       }, "Return", function () awful.spawn(terminal) end, {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey,       }, "b", function () awful.spawn(web_browser) end, {description = "Open Web Browser", group = "launcher"}),    
    awful.key({ ctrl, shift   }, "f", function () awful.spawn(file_manager) end, {description = "File Manager", group = "launcher"}),
    awful.key({ modkey, shift }, "Return", function () awful.spawn("xterm") end, {description = "open xterm", group = "launcher"}),
})

-- Focus related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,      }, "Up", function () awful.client.focus.byidx(1) end, {description = "focus next by index", group = "client"}),
    awful.key({ modkey,      }, "Down", function () awful.client.focus.byidx(-1) end, {description = "focus previous by index", group = "client"}),
    awful.key({ modkey,      }, "Tab", function () awful.client.focus.history.previous()
        if client.focus then
            client.focus:raise()
        end
    end, {description = "go back", group = "client"}),
    awful.key({ modkey, ctrl }, "Up", function () awful.screen.focus_relative( 1) end, {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, ctrl }, "Down", function () awful.screen.focus_relative(-1) end, {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey, ctrl }, "n", function ()
        local c = awful.client.restore()
        if c then
            c:activate { raise = true, context = "key.unminimize" }
        end
    end, {description = "restore minimized", group = "client"}),
})

-- Layout related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey, shift }, "Left", function () awful.client.swap.byidx( 1) end, {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, shift }, "Right", function () awful.client.swap.byidx(-1) end, {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey,       }, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,       }, "Left", function () awful.tag.incmwfact(-0.05) end, {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey,       }, "Right", function () awful.tag.incmwfact( 0.05) end, {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey, shift }, "Up", function () awful.tag.incnmaster( 1, nil, true) end, {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, shift }, "Down", function () awful.tag.incnmaster(-1, nil, true) end, {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, ctrl  }, "Left", function () awful.tag.incncol(-1, nil, true) end, {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey, ctrl  }, "Right", function () awful.tag.incncol( 1, nil, true) end, {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey,       }, "space", function () awful.layout.inc( 1) end, {description = "select next", group = "layout"}),
    awful.key({ modkey, shift }, "space", function () awful.layout.inc(-1) end, {description = "select previous", group = "layout"}),
})

-- Media Control :
awful.keyboard.append_global_keybindings({
    awful.key({}, "XF86AudioLowerVolume", function () awful.spawn("amixer -q -D pulse sset Master 5%-", false) end),
    awful.key({}, "XF86AudioRaiseVolume", function () awful.spawn("amixer -q -D pulse sset Master 5%+", false) end),
    awful.key({}, "XF86AudioMute", function () awful.spawn("amixer -D pulse set Master 1+ toggle", false) end),
    awful.key({}, "XF86AudioPlay", function() awful.spawn("playerctl play-pause", false) end),
    awful.key({}, "XF86AudioNext", function() awful.spawn("playerctl next", false) end),
    awful.key({}, "XF86AudioPrev", function() awful.spawn("playerctl previous", false) end),
    awful.key({}, "XF86MonBrightnessUp", function() awful.spawn("brightnessctl set 5%+", false) end),
    awful.key({}, "XF86MonBrightnessDown", function() awful.spawn("brightnessctl set 5%-", false) end),
})

-- Standard program :
awful.keyboard.append_global_keybindings({
    awful.key({ }, "Print", function () awful.spawn("screenshot") end, {description = "Take Screenshot", group = "screenshot"}),
    awful.key({ modkey }, "Print", function () awful.spawn("screenshot-select") end, {description = "Take Selective Screenshot", group = "screenshot"}),
    awful.key({ modkey }, "p", function () awful.spawn("rofi -show drun -show-icons &>> /tmp/rofi.log") end, {description = "Rofi Launcher", group = "launcher"}),
    awful.key({ modkey }, "Insert", function () awful.spawn("clipmenu") end, {description = "Clipboard History", group = "utility"}),
    awful.key({ modkey }, "y", awful.placement.centered, {description = "Center window", group = "client"}),

    awful.key({ modkey }, "=", function ()
        for s in screen do
            s.mywibar.visible = not s.mywibar.visible
        end
    end, {description = "toggle wibar", group = "awesome"}),

    awful.key({ alt }, "Tab", function() awesome.emit_signal("sidebar::toggle") end),
    awful.key({ alt }, "t", function() awful.titlebar.toggle(client.focus) end),
})

-- Client :
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        -- Fullscreen toggle
        awful.key({ modkey }, "f", function(c) c.fullscreen = not c.fullscreen; c:raise() end),
        -- Close window
        awful.key({ modkey, shift }, "c", function(c) c:kill() end),
        -- Toggle floating
        awful.key({ modkey,  	 }, "v", awful.client.floating.toggle),
        -- Move to master
        awful.key({ modkey, ctrl }, "Return", function(c) c:swap(awful.client.getmaster()) end),
        -- Move to screen
        awful.key({ modkey }, "o", function(c) c:move_to_screen() end),
        -- Toggle on top
        awful.key({ modkey }, "t", function(c) c.ontop = not c.ontop end),
        -- Minimize
        awful.key({ modkey }, "n", function(c) c.minimized = true end),
        -- Maximize toggles
        awful.key({ modkey }, "m", function(c) c.maximized = not c.maximized; c:raise() end),
        awful.key({ modkey, ctrl }, "m", function(c) c.maximized_vertical = not c.maximized_vertical; c:raise() end),
        awful.key({ modkey, shift }, "m", function(c) c.maximized_horizontal = not c.maximized_horizontal; c:raise() end),
    })
end)

-- Tags related keybindings
awful.keyboard.append_global_keybindings({
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numrow",
        description = "only view tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                tag:view_only()
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, ctrl },
        keygroup    = "numrow",
        description = "toggle tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end,
    },
    awful.key {
        modifiers = { modkey, shift },
        keygroup    = "numrow",
        description = "move focused client to tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, ctrl, shift },
        keygroup    = "numrow",
        description = "toggle focused client on tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numpad",
        description = "select layout directly",
        group       = "layout",
        on_press    = function (index)
            local t = awful.screen.focused().selected_tag
            if t then
                t.layout = t.layouts[index] or t.layout
            end
        end,
    }
})
