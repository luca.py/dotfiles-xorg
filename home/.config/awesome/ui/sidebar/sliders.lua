-- ## Sliders ##
-- ~~~~~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require ("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

-- Create volume slider :
local volume_icon = wibox.widget{
    font   	= theme.sidebar_font,
    markup 	= helpers.colorize_text("", colors.blue),
    widget 	= wibox.widget.textbox,
    valign 	= "center",
    align 	= "center"
}

local volume_osd_value = wibox.widget({
	text = "0%",
	font = theme.font,
	align = "center",
	valign = "center",
	widget = wibox.widget.textbox,
})

local volume_slider = wibox.widget {
	forced_width = dpi(220),
	forced_height = dpi(10),
	bar_shape = helpers.rrect(dpi(12)),
	bar_height = dpi(14),
	bar_color = colors.black,
	bar_active_color = colors.blue,
	handle_shape = gears.shape.circle,
	handle_color = colors.brightblue,
	handle_width = dpi(20),
	widget = wibox.widget.slider
}

volume = wibox.widget {
	nil,
	{
		volume_icon,
		volume_slider,
		volume_osd_value,
		spacing = dpi(20),
		layout = wibox.layout.fixed.horizontal,
	},
	expand = "none",
	layout = wibox.layout.align.vertical,
}

local update_volume = function() 
	awful.spawn.easy_async_with_shell("pamixer --get-volume", function(stdout) 
		volume_slider.value = tonumber(stdout:match("%d+"))
	end)
end

volume_slider:connect_signal("property::value", function(_, vol) 
	awful.spawn("pamixer --set-volume ".. vol, false)
	-- Update textbox widget text
	volume_osd_value.text = vol .. "%"
	awesome.emit_signal("module::volume_osd_value", vol)
end)

-- Create Mic slider :
local mic_icon = wibox.widget{
    font   	= theme.sidebar_font,
    markup 	= helpers.colorize_text("  ", colors.green),
    widget 	= wibox.widget.textbox,
    valign 	= "center",
    align 	= "center"
}

local mic_osd_value = wibox.widget({
	text = "0%",
	font = theme.font,
	align = "center",
	valign = "center",
	widget = wibox.widget.textbox,
})

local mic_slider = wibox.widget {
	forced_width = dpi(220),
	forced_height = dpi(10),
	bar_shape = helpers.rrect(dpi(12)),
	bar_height = dpi(14),
	bar_color = colors.black,
	bar_active_color = colors.green,
	handle_shape = gears.shape.circle,
	handle_color = colors.brightgreen,
	handle_width = dpi(20),
	widget = wibox.widget.slider
}

local mic = wibox.widget {
	nil,
	{
		mic_icon,
		mic_slider,
		mic_osd_value,
		spacing = dpi(20),
		layout = wibox.layout.fixed.horizontal,
	},
	expand = "none",
	layout = wibox.layout.align.vertical,
}

local update_mic = function() 
	awful.spawn.easy_async_with_shell("pamixer --default-source --get-volume", function(stdout) 
		mic_slider.value = tonumber(stdout:match("%d+"))
	end)
end

mic_slider:connect_signal("property::value", function(_, mic_vol) 
	awful.spawn("pamixer --default-source --set-volume ".. mic_vol, false)
	-- Update textbox widget text
	mic_osd_value.text = mic_vol .. "%"
	awesome.emit_signal("module::mic_osd_value", mic_vol)
end)


gears.timer {
	timeout = 10,
	autostart = true,
	call_now = true,
	callback = function() 
		update_volume()
		update_mic()
	end
}

local all_slider = wibox.widget {
	volume,
	mic,
	spacing = dpi(10),
	layout = wibox.layout.fixed.vertical,
}

return all_slider
