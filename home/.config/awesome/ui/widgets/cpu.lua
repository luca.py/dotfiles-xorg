-- ## Cpu ##
-- ~~~~~~~~~

-- Requirements :
-- ~~~~~~~~~~~~~~
local gears = require("gears")
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local theme = require("beautiful")
local dpi = require('beautiful').xresources.apply_dpi

-- # Libs :
-- ~~~~~~~~
local helpers = require("libs.helpers")

local CMD = [[sh -c "grep '^cpu.' /proc/stat; ps -eo 'pid:10,pcpu:5,pmem:5,comm:30,cmd' --sort=-pcpu ]]
    .. [[| grep -v [p]s | grep -v [g]rep | head -11 | tail -n +2"]]

-- A smaller command, less resource intensive, used when popup is not shown.
local CMD_slim = [[grep --max-count=1 '^cpu.' /proc/stat]]

local HOME_DIR = os.getenv("HOME")

local cpu_widget = {}
local cpu_rows = {
    spacing = 4,
    layout = wibox.layout.fixed.vertical,
}
local is_update = true
local process_rows = {
    layout = wibox.layout.fixed.vertical,
}

-- Remove spaces at end and beggining of a string
local function trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

-- Checks if a string starts with a another string
local function starts_with(str, start)
    return str:sub(1, #start) == start
end


local function create_textbox(args)
    return wibox.widget{
        text = args.text,
        align = args.align or 'left',
        markup = args.markup,
        forced_width = args.forced_width or 70,
        widget = wibox.widget.textbox
    }
end


local function worker(user_args)
    local args = user_args or {}
    local timeout = args.timeout or 1

    local cpu = wibox.widget.textbox()
    local total_prev, idle_prev = 0, 0

    -- Watch for slim CPU data (no popup)
    watch([[bash -c "cat /proc/stat | grep '^cpu '"]], 2, function(_, stdout)
        local user, nice, system, idle, iowait, irq, softirq, steal =
            stdout:match('(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)')

        local total = user + nice + system + idle + iowait + irq + softirq + steal
        local diff_idle = idle - idle_prev
        local diff_total = total - total_prev
        local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10

        total_prev, idle_prev = total, idle
        cpu.text = math.floor(diff_usage) .. '%'
        if diff_usage < 10 then cpu.text = '0' .. cpu.text end
        cpu:set_markup(helpers.colorize_text(cpu.text, colors.brightgreen))

        collectgarbage('collect')
    end)

	-- Icon :
	local widget_icon = " "
	local icon = wibox.widget{
		font   	= theme.icon_font,
		markup 	= helpers.colorize_text(widget_icon, colors.brightgreen),
		widget 	= wibox.widget.textbox,
		valign 	= "center",
		align 	= "center"
	}

    -- This timer periodically executes the heavy command while the popup is open.
    -- It is stopped when the popup is closed and only the slim command is run then.
    -- This greatly improves performance while the popup is closed at the small cost
    -- of a slightly longer popup opening time.
    local popup_timer = gears.timer {
        timeout = timeout
    }

    local popup = awful.popup{
        ontop = true,
        visible = false,
        shape = gears.shape.rounded_rect,
        border_width = 1,
        border_color = theme.bg_normal,
        maximum_width = 400,
        offset = { y = -10 },
        widget = {}
    }

    -- Do not update process rows when mouse cursor is over the widget
    popup:connect_signal("mouse::enter", function() is_update = false end)
    popup:connect_signal("mouse::leave", function() is_update = true end)

    cpu:buttons(
		awful.util.table.join(
			awful.button({}, 1, function()
				if popup.visible then
					popup.visible = not popup.visible
					-- When the popup is not visible, stop the timer
					popup_timer:stop()
				else
					popup:move_next_to(mouse.current_widget_geometry)
					-- Restart the timer, when the popup becomes visible
					-- Emit the signal to start the timer directly and not wait the timeout first
					popup_timer:start()
					popup_timer:emit_signal("timeout")
				end
			end)
		)
    )

    --- By default graph widget goes from left to right, so we mirror it and push up a bit
    cpu_widget = wibox.widget {
        {
            icon,
            cpu,
            layout = wibox.layout.fixed.horizontal
        },
        bottom = 2,
        color = background_color,
        widget = wibox.container.margin
    }

    -- This part runs constantly, also when the popup is closed.
    -- It updates the graph widget in the bar.
    local maincpu = {}
    watch(CMD_slim, timeout, function(widget, stdout)

        local _, user, nice, system, idle, iowait, irq, softirq, steal, _, _ =
            stdout:match('(%w+)%s+(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)')

        local total = user + nice + system + idle + iowait + irq + softirq + steal

        local diff_idle = idle - tonumber(maincpu['idle_prev'] == nil and 0 or maincpu['idle_prev'])
        local diff_total = total - tonumber(maincpu['total_prev'] == nil and 0 or maincpu['total_prev'])
        local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10

        maincpu['total_prev'] = total
        maincpu['idle_prev'] = idle

        widget:add_value(diff_usage)
    end,
    cpu
    )

    -- This part runs whenever the timer is fired.
    -- It therefore only runs when the popup is open.
    local cpus = {}
    popup_timer:connect_signal('timeout', function()
        awful.spawn.easy_async(CMD, function(stdout, _, _, _)
            local i = 1
            local j = 1
            for line in stdout:gmatch("[^\r\n]+") do
                if starts_with(line, 'cpu') then

                    if cpus[i] == nil then cpus[i] = {} end

                    local name, user, nice, system, idle, iowait, irq, softirq, steal, _, _ =
                        line:match('(%w+)%s+(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)')

                    local total = user + nice + system + idle + iowait + irq + softirq + steal

                    local diff_idle = idle - tonumber(cpus[i]['idle_prev'] == nil and 0 or cpus[i]['idle_prev'])
                    local diff_total = total - tonumber(cpus[i]['total_prev'] == nil and 0 or cpus[i]['total_prev'])
                    local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10

                    cpus[i]['total_prev'] = total
                    cpus[i]['idle_prev'] = idle

                    local row = wibox.widget
                    {
                        create_textbox{text = name},
                        create_textbox{text = math.floor(diff_usage) .. '%'},
                        {
                            max_value = 100,
                            value = diff_usage,
                            forced_height = 20,
                            forced_width = 150,
                            paddings = 1,
                            margins = 4,
                            border_width = 1,
                            border_color = theme.bg_focus,
                            background_color = theme.bg_normal,
                            bar_border_width = 1,
                            bar_border_color = theme.bg_focus,
                            color = "linear:150,0:0,0:0,#D08770:0.3,#BF616A:0.6," .. theme.fg_normal,
                            widget = wibox.widget.progressbar,

                        },
                        layout  = wibox.layout.ratio.horizontal
                    }
                    row:ajust_ratio(2, 0.15, 0.15, 0.7)
                    cpu_rows[i] = row
                    i = i + 1
					process_rows[j] = row
					j = j + 1
                end
            end
            
            popup:setup {
                {
                    cpu_rows,
                    {
                        orientation = 'horizontal',
                        forced_height = 0,
                        color = theme.bg_focus,
                        widget = wibox.widget.separator
                    },

                    layout = wibox.layout.fixed.vertical,
                },
                margins = 8,
                widget = wibox.container.margin
            }
        end)
    end)
    return cpu_widget
end

return setmetatable(cpu_widget, { __call = function(_, ...)
    return worker(...)
end })
