## **Hello there**
Just run the install script and hopefully every thing will be good :)
Thanks for stopping by 💜


| task              | name                   |
| ----------------- | ---------------------- |
| wm                | [awesome-git](https://github.com/awesomeWM/awesome)                                      |
| terminal          | [Kitty](https://github.com/alacritty/alacritty)                                      |
| music player      | [audacious](https://audacious-media-player.org/)    |
| bar               | [wibar](https://awesomewm.org/apidoc/popups_and_bars/awful.wibar.html)                   |
| compositor        | [compfy](https://github.com/allusive-dev/compfy)                                  | 

## **Nots :**

 - I use `doas` instead of `sudo` you may want to change that !.
 - to have my full system setup check my arch install script : [arch
   installer](https://gitlab.com/luca.py/arch-installer.git) .
 - to get the latest weather updates you need
   [openweathermap](https://openweathermap.org/) api key .
 - open rofi and type Theme-changer so you can change awesomewm, rofi,
   Gtk and kitty theme


## **Credits**
**Special thanks to 💜** 

 - [Rklyz](https://github.com/rklyz/)
 - [saimoomedits](https://github.com/saimoomedits/)
